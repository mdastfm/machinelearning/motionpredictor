import unittest
import api.app as gesture_predictor_api
import json
from pathlib import Path
import os


class TestMotionPredictor(unittest.TestCase):

    def test_gesture_predictor(self):
        file = os.path.join(Path(__file__).parent, 'loads/move_up.json')
        with open(file) as json_file:
            data = json.load(json_file)
        response = gesture_predictor_api.predict_json(data)
        self.assertEqual(response, 'MOVE_UP')

    if __name__ == '__main__':
        unittest.main()
