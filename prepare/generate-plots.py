import pandas as pd
import os

root_dir = os.path.abspath('../')
dir_data = os.path.join(root_dir, 'data')
dir_csv = os.path.join(dir_data, 'csv')
dir_training = os.path.join(dir_data, 'training')

if not os.path.exists(dir_training):
    os.mkdir(dir_training)

for filename in os.listdir(dir_csv):

    if not filename.endswith('.csv'):
        continue

    full_path_to_file = os.path.join(dir_csv, filename)
    movement_folder = os.path.splitext(filename)[0].lower().split('_')[0]
    full_path_to_movement_folder = os.path.join(dir_training, movement_folder)

    if not os.path.exists(full_path_to_movement_folder):
        os.mkdir(full_path_to_movement_folder)

    image = full_path_to_movement_folder + '/' + os.path.splitext(filename)[0].lower() + '.png'

    if os.path.exists(image):
        continue

    data = pd.read_csv(full_path_to_file, names=['timestamp', 'x', 'y', 'z'])
    plot = data.plot('timestamp')
    fig = plot.get_figure()
    fig.savefig(image)

    print(image)





